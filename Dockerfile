FROM node:8.16-alpine

WORKDIR /usr/src/app

COPY ./api .

RUN apk add --no-cache tzdata

RUN npm install &&  npm -g install nodemon

EXPOSE 3002

CMD ["nodemon", "bin/www"]
