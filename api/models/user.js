'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let user = new Schema({
  id : Number,
  nombre : String,
  apellido : String,
  edad : Number,
  email: String
});

module.exports = mongoose.model('User', user);
