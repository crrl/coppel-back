'use strict';
let express = require('express');
let router = express.Router();

let user = require('../models/user');

router.post('/api/user', (req, res) => {
  if (!req.body.apellido || !req.body.nombre ||
      !req.body.email || !req.body.edad) {
    res.status(400).send({'error': 'Faltan datos.'});
    return;
  }
  user.findOne(null, null, {sort: {'id': -1}}, (err, result) => {
    let id = 1;
    if (result) {
      id = result.id + 1;
    }
    let newUser = new user({
      id,
      nombre : req.body.nombre,
      apellido : req.body.apellido,
      edad: req.body.edad,
      email: req.body.email
    });

    newUser.save((err) => {
      if (err) {
        res.write("Ha ocurrido un error.");
      }
    })
    .then( () => {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('Se ha agregado con éxito.');
    });
      
  });
});


router.get('/api/user', (req, res) => {
 user.find(function(err, result) {
   if (err) {
    res.write("Ha ocurrido un error.");
   } else {
      user.count((err,count) => {
        if (err) {
          res.write(err);
        } else {
          res.send({result,count});          
        }
      });
    }
  })
  .sort([['id', 1]])
  .skip(parseInt(req.query.skip))
  .limit(parseInt(6));
});

router.get('/api/user/:id', (req, res) => {
 user.findOne({'id':req.params.id})
 .then(function (response) {
   res.send(response);
 })
 .catch(function() {
    res.writeHead(400, {'Content-Type':'text/plain'});
    res.end('No hay usuarios con esa clave.');
  });
});



router.put('/api/user/:id', (req, res) => {
  user.update({'id': req.params.id}, req.body, (err, values) => {
      if (!err) {
          res.json("Se ha actualizado correctamente.");
      } else {
          res.write("Ha ocurrido un error.");
      }
  });
});

router.delete('/api/user/:id', (req, res) => {
  user.remove({'id': req.params.id},function(err) {
    if (!err) {
        res.json("Se ha eliminado correctamente.");
    } else {
        res.write("Ha ocurrido un error.");
    }
  });
});

module.exports = router;